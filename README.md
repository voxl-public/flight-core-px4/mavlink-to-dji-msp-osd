# mavlink_to_DJI_MSP_OSD

Converts MAVLink coming from Flight Core to DJI MSP OSD to be displayed on FPV goggles. This project uses an arduino for the translation. 

Inspiration project on [Github](https://github.com/d3ngit/djihdfpv_mavlink_to_msp_V2)

# Setup

1. Clone the project: voxl / Flight Core and PX4 / DJI OSD 
```git clone git@gitlab.com:voxl-public/flight-core-px4/mavlink-to-dji-msp-osd.git```

1. Install Arduino IDE for Linux: [Arduino IDE 1 Installation (Linux)](https://docs.arduino.cc/software/ide-v1/tutorials/Linux)

1. Install the Teensyduino Arduino IDE Add-on: [Teensyduino: Download and Install Teensy support into the Arduino IDE](https://www.pjrc.com/teensy/td_download.html)

1. To find Arduino installation directory: ```readlink `whereis arduino` ```

1. Copy all of the Arduino_Libraries directories from the cloned project into the <Arduino Install Path>Arduino/libraries. 

# Code

The software currently uses MAVLink v1

# Hardware

## Teensy LC

[Teensy LC Information](https://www.pjrc.com/store/teensylc.html)

![](images/teensy-pinout-top.png)
![](images/teensy-pinout-bottom.png)

## DJI Air Unit

[DJI Air Unit Information](https://cdn.shopify.com/s/files/1/0036/3921/4169/files/air_unit_kit_manual.pdf?v=1631863310)

![](images/dji-air-unit-pinout.png)

## DJI Goggles

[DJI Goggles V2 Information](https://store.dji.com/product/dji-fpv-goggles-v2?from=menu_products)

Firmware Version: 01.00.0606

SN: 37SBJ5B00100AM

![](images/dji-goggles-v2.png)

### Settings

Set the goggles for Custom OSD via the Settings menu: Settings -> Display -> Custom OSD - ON

## Flight Core

[Flight Core Information](https://docs.modalai.com/flight-core-datasheets-connectors/#j5---telemetry-connector)

![](images/flight-core-pinout.png)

![](images/flight-core-pinout-j5.png)

## Connection

### Flight Core to Arduino

| Flight Core     | Arduino Teensy LC | Notes                                |
|-----------------|-------------------|--------------------------------------|
| Pin 1, 5V       | Vin               | Vin → 3.7 to 5.5v                    |
| Pin 2, TX, 3.3v | Pin 0, RX, 3.3v   | Arduino HW Serial1, Baud Rate 115200 |
| Pin 3, RX, 3.3v | Pin 1, TX, 3.3v   | Arduino HW Serial1, Baud Rate 115200 |
| Pin 6, GND      | GND               |                                      |

Configure Flight Core SER_TEL1_BAUD parameter to connect to Arduino Teensy: 

![](images/baud-parameter.png)

### Arduino to DJI Air Unit


| DJI Air Unit            | Arduino Teensy LC | Notes                                |
|-------------------------|-------------------|--------------------------------------|
| Vin (Red)               | Vin               | Vin → 7.4 to 17.6v                   |
| GND (Black)             | GND               | Arduino HW Serial1, Baud Rate 115200 |
| Pin 3, RX, 3.3v (White) | Pin 10, TX, 3.3v  | Arduino HW Serial2, Baud Rate 115200 |


 
